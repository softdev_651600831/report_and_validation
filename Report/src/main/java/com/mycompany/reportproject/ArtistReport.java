/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HP
 */
public class ArtistReport {
    private int id;
    private String name;
    private int TotalQuantity;
    private float TotalPrice;

    public ArtistReport(int id, String name, int TotalQuantity, float TotalPrice) {
        this.id = id;
        this.name = name;
        this.TotalQuantity = TotalQuantity;
        this.TotalPrice = TotalPrice;
    }

    public ArtistReport(String name, int TotalQuantity, float TotalPrice) {
        this.id = -1;
        this.name = name;
        this.TotalQuantity = TotalQuantity;
        this.TotalPrice = TotalPrice;
    }
    
     public ArtistReport() {
        this(-1,"", 0 , 0);
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQuantity() {
        return TotalQuantity;
    }

    public void setTotalQuantity(int TotalQuantity) {
        this.TotalQuantity = TotalQuantity;
    }

    public float getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(float TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    @Override
    public String toString() {
        return "ArtistReport{" + "id=" + id + ", name=" + name + ", TotalQuantity=" + TotalQuantity + ", TotalPrice=" + TotalPrice + '}';
    }
     
     public static ArtistReport fromRS(ResultSet rs) {
        ArtistReport artistReport = new ArtistReport();
        try {
            artistReport.setId(rs.getInt("ArtistId"));
            artistReport.setName(rs.getString("Name"));
            artistReport.setTotalQuantity(rs.getInt("TotalQuantity"));
            artistReport.setTotalPrice(rs.getFloat("TotalPrice"));
         
        } catch (SQLException ex) {
            Logger.getLogger(ArtistReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return artistReport;
    }
    
    
}
